CREATE TABLE "Vote" (
  "id" integer PRIMARY KEY,
  "Annee" integer,
  "Code_du_departement" integer,
  "Inscrits" integer,
  "Abstentions" integer,
  "Votants" integer,
  "Blancs_et_nuls" integer
);

CREATE TABLE "Candidat" (
  "id" integer PRIMARY KEY,
  "Annee" integer,
  "Code_du_departement" integer,
  "Partie" varchar,
  "Nb_Vote" varchar
);

CREATE TABLE "fait" (
  "id" integer PRIMARY KEY,
  "Annee" integer
);

ALTER TABLE "Candidat" ADD FOREIGN KEY ("Annee") REFERENCES "fait" ("Annee");

ALTER TABLE "Vote" ADD FOREIGN KEY ("Annee") REFERENCES "fait" ("Annee");
